//
//  AppDelegate.m
//  SampleProtocol
//
//  Created by Sarada Adhikarla on 11/6/15.
//  Copyright © 2015 Sarada Adhikarla. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self registerForLocalNotification: application];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    NSDate *alarmTime = [[NSDate date]dateByAddingTimeInterval:5];
    UIApplication *app = [UIApplication sharedApplication];
    UILocalNotification *notify = [[UILocalNotification alloc]init];
    if (notify) {
        
        notify.timeZone = [NSTimeZone defaultTimeZone];
        notify.fireDate = alarmTime;
        notify.repeatInterval = 0;
        notify.alertBody = @"It's been too long since you have opened CALCULATOR app";
        notify.alertTitle = @"Calculator";
        notify.hasAction = YES;
        notify.category = @"CATEGORY_ID";
        notify.applicationIconBadgeNumber = 4;
        notify.soundName = UILocalNotificationDefaultSoundName;
        notify.alertLaunchImage = @"images.jpeg";
        [app scheduleLocalNotification:notify];
        
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *notifications = [app scheduledLocalNotifications];
    if ([notifications count] > 0) {
        [app cancelAllLocalNotifications];
    }
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    NSLog(@"****** In callback for notification ****");
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void) registerForLocalNotification: (UIApplication *)application {
    
    // Action 1
    UIMutableUserNotificationAction *openAction = [[UIMutableUserNotificationAction alloc]init];
    
    // Define an ID string to be passed back to your app when you handle the action
    openAction.identifier = @"OPEN_IDENTIFIER";
    
    // Localized string displayed in the action button
    openAction.title = @"Open";
    
    // If you need to show UI, choose foreground
    openAction.activationMode = UIUserNotificationActivationModeForeground;
    
    // Set whether the action requires the user to authenticate
    openAction.authenticationRequired = NO;
    
    // Destructive actions display in red
    openAction.destructive = NO;
    
    // Action 2
    UIMutableUserNotificationAction *closeAction = [[UIMutableUserNotificationAction alloc]init];
    
    // Define an ID string to be passed back to your app when you handle the action
    closeAction.identifier = @"CLOSE_IDENTIFIER";
    
    // Localized string displayed in the action button
    closeAction.title = @"Exit";
    
    // If you need to show UI, choose foreground
    closeAction.activationMode = UIUserNotificationActivationModeForeground;
    
    // Set whether the action requires the user to authenticate
    closeAction.authenticationRequired = NO;
    
    // Destructive actions display in red
    closeAction.destructive = NO;
    
    
    //Category
    UIMutableUserNotificationCategory *category = [[UIMutableUserNotificationCategory alloc]init];
    
    //Define ID for the category
    category.identifier = @"CATEGORY_ID";
    
    //set the action-1 to the category
    [category setActions:@[closeAction, openAction] forContext:UIUserNotificationActionContextDefault];
    
    //Group the categories
    NSSet *categories = [NSSet setWithObject:category];
    
    //Define the types
    UIUserNotificationType types = (UIUserNotificationTypeAlert|
                                    UIUserNotificationTypeSound |
                                    UIUserNotificationTypeBadge
                                    );
    
    //register the notification with the defined setting types and categories
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:types categories:categories]];
    }

}

-(void) application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    NSLog(@"In didRegisterUserNotificationSettings");
}

- (void) application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification completionHandler:(void (^)())completionHandler {
    if ([identifier  isEqual: @"OPEN_IDENTIFIER"]) {
        
    } else if([identifier isEqual:@"CLOSE_IDENTIFIER"]){

        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Warning"
                                                                       message:@"Are you sure you want to exit the app?"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
            exit(0);
        }];
        [alert addAction:yesAction];
        UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {}];
        [alert addAction:noAction];
    
        [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
    }
    
}
@end
