//
//  ViewController.m
//  SampleProtocol
//
//  Created by Sarada Adhikarla on 11/6/15.
//  Copyright © 2015 Sarada Adhikarla. All rights reserved.
//

#import "ViewController.h"

const int PLUS = 100;
const int MINUS = 101;
const int STAR = 102;
const int BY = 103;
const int EQUALTO = 104;

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    MyCustomView *custView = [[MyCustomView alloc]initWithFrame:CGRectMake(250,100,500,567)];
    custView.delegate = self;
    [self.view addSubview:custView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom delegate methods -

-(void)myCustomView:(MyCustomView *)myCustomView onPressingNumberButton:(UIButton *)oneButton {
    [self myCustomView:myCustomView setTextToLabel:oneButton];
}

-(void)myCustomView:(MyCustomView *)myCustomView onPressingOperatorButton:(UIButton *)operatorButton {
    myCustomView.delegate = self;
    myCustomView.textField.textAlignment = NSTextAlignmentRight;
    [self doCalculation];
    method = [operatorButton tag];
}

-(void)myCustomView:(MyCustomView *)myCustomView onPressingEqualToButton:(UIButton *)equalToButton {
    myCustomView.delegate = self;
    myCustomView.textField.textAlignment = NSTextAlignmentRight;
    [self doCalculation];
    method = EQUALTO;
    myCustomView.textField.text = [NSString stringWithFormat:@"%.2f",currentTotal];
    
}

-(void)myCustomView:(MyCustomView *)myCustomView onPressingClearButton:(UIButton *)clearButton {
    myCustomView.delegate = self;
    myCustomView.textField.textAlignment = NSTextAlignmentRight;
    method = 0;
    selectedNumber = 0;
    currentTotal = 0;
    myCustomView.textField.text = [NSString stringWithFormat:@"0"];
}

#pragma mark - instance methods for logic -

-(void)doCalculation {
    if (currentTotal == 0) {
        currentTotal = selectedNumber;
    }
    else {
        switch (method) {
            case PLUS:
                currentTotal = currentTotal + selectedNumber;
                break;
            case MINUS:
                currentTotal = currentTotal - selectedNumber;
                break;
            case STAR:
                currentTotal = currentTotal * selectedNumber;
                break;
            case BY:
                currentTotal = currentTotal / selectedNumber;
                break;
            default:
                break;
        }
    }
    selectedNumber = 0;
}

-(void)myCustomView:(MyCustomView *)myCustomView setTextToLabel:(UIButton *)button {
    myCustomView.delegate = self;
    myCustomView.textField.textAlignment = NSTextAlignmentRight;
    selectedNumber = selectedNumber * 10;
    selectedNumber = selectedNumber +(int) [button tag];
    myCustomView.textField.text = [NSString  stringWithFormat:@"%i",selectedNumber];

}
@end
