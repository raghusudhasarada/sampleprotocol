//
//  ViewController.h
//  SampleProtocol
//
//  Created by Sarada Adhikarla on 11/6/15.
//  Copyright © 2015 Sarada Adhikarla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyCustomView.h"

extern const int PLUS;
extern const int MINUS;
extern const int STAR;
extern const int BY;
extern const int EQUALTO;

int selectedNumber;
NSInteger method;
float currentTotal;

@interface ViewController : UIViewController <MyCalculatorDelegate, UITextViewDelegate>

-(void)doCalculation;
-(void)myCustomView:(MyCustomView *)myCustomView setTextToLabel:(UIButton *)button ;

@end

