//
//  MyCustomView.m
//  SampleProtocol
//
//  Created by Sarada Adhikarla on 11/6/15.
//  Copyright © 2015 Sarada Adhikarla. All rights reserved.
//

//test

#import "MyCustomView.h"

@implementation MyCustomView


-(instancetype) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self) {
        self = (MyCustomView *)[[[NSBundle mainBundle]loadNibNamed:@"MyCustomView" owner:self options:nil]firstObject];
        [self setFrame:frame];
    }
    return self;
}

- (IBAction)onPressingOneButton:(UIButton *)sender {
    [self callRespectiveDelegateMethod:sender];
}

- (IBAction)onPressingTwoButton:(UIButton *)sender {
    [self callRespectiveDelegateMethod:sender];
}

- (IBAction)onPressingThreeButton:(UIButton *)sender {
    [self callRespectiveDelegateMethod:sender];
}

- (IBAction)onPressingFourButton:(UIButton *)sender {
    [self callRespectiveDelegateMethod:sender];
}

- (IBAction)onPressingFiveButton:(UIButton *)sender {
    [self callRespectiveDelegateMethod:sender];
}

- (IBAction)onPressingSixButton:(UIButton *)sender {
    [self callRespectiveDelegateMethod:sender];
}

- (IBAction)onPressingSevenButton:(UIButton *)sender {
    [self callRespectiveDelegateMethod:sender];
}

- (IBAction)onPressingEightButton:(UIButton *)sender {
    [self callRespectiveDelegateMethod:sender];
}

- (IBAction)onPressingNineButton:(UIButton *)sender {
    [self callRespectiveDelegateMethod:sender];
}

- (IBAction)onPressingZeroButton:(UIButton *)sender {
    [self callRespectiveDelegateMethod:sender];
}

- (IBAction)onPressingEqualToButton:(UIButton *)sender {
    [self callRespectiveDelegateMethod:sender];
}

- (IBAction)onPressingPlusButton:(UIButton *)sender {
    [self callRespectiveDelegateMethod:sender];
}

- (IBAction)onPressingMinusButton:(UIButton *)sender {
    [self callRespectiveDelegateMethod:sender];
}

- (IBAction)onPressingStarButton:(UIButton *)sender {
    [self callRespectiveDelegateMethod:sender];
}

- (IBAction)onPressingByButton:(UIButton *)sender {
    [self callRespectiveDelegateMethod:sender];
}

- (IBAction)onPressingClearButton:(UIButton *)sender {
    [self callRespectiveDelegateMethod:sender];
}

-(void)callRespectiveDelegateMethod: (UIButton *)sender {
    switch ([sender tag]) {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        {
            if (self.delegate && [self.delegate respondsToSelector:@selector(myCustomView:onPressingNumberButton:)]) {
                [self.delegate myCustomView:self onPressingNumberButton:sender];
            }
        }
            break;
        case 100:
        case 101:
        case 102:
        case 103:
        {
            if (self.delegate && [self.delegate respondsToSelector:@selector(myCustomView:onPressingOperatorButton:)]) {
                [self.delegate myCustomView:self onPressingOperatorButton:sender];
            }
        }
            break;
        case 104:
        {
            if (self.delegate && [self.delegate respondsToSelector:@selector(myCustomView:onPressingEqualToButton:)]) {
                [self.delegate myCustomView:self onPressingEqualToButton:sender];
            }
        }
            break;
        
        case 105:
        {
            if (self.delegate && [self.delegate respondsToSelector:@selector(myCustomView:onPressingClearButton:)]) {
                [self.delegate myCustomView:self onPressingClearButton:sender];
            }
        }
            break;
        default:
            break;
    }
    
}

@end
