//
//  MyCustomView.h
//  SampleProtocol
//
//  Created by Sarada Adhikarla on 11/6/15.
//  Copyright © 2015 Sarada Adhikarla. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MyCustomView;
@protocol MyCalculatorDelegate <NSObject>

@optional
-(void)myCustomView:(MyCustomView *)myCustomView onPressingNumberButton:(UIButton *) Button;
-(void)myCustomView:(MyCustomView *)myCustomView onPressingOperatorButton: (UIButton *) operatorButton;
-(void)myCustomView:(MyCustomView *)myCustomView onPressingEqualToButton: (UIButton *) equalToButton;
-(void)myCustomView:(MyCustomView *)myCustomView onPressingClearButton: (UIButton *) clearButton;

@end

@interface MyCustomView : UIView
- (instancetype) initWithFrame: (CGRect) frame;
@property (nonatomic)id <MyCalculatorDelegate> delegate;
- (IBAction)onPressingOneButton:(UIButton *)sender;
- (IBAction)onPressingTwoButton:(UIButton *)sender;
- (IBAction)onPressingThreeButton:(UIButton *)sender;
- (IBAction)onPressingFourButton:(UIButton *)sender;
- (IBAction)onPressingFiveButton:(UIButton *)sender;
- (IBAction)onPressingSixButton:(UIButton *)sender;
- (IBAction)onPressingSevenButton:(UIButton *)sender;
- (IBAction)onPressingEightButton:(UIButton *)sender;
- (IBAction)onPressingNineButton:(UIButton *)sender;
- (IBAction)onPressingZeroButton:(UIButton *)sender;
- (IBAction)onPressingEqualToButton:(UIButton *)sender;
- (IBAction)onPressingPlusButton:(UIButton *)sender;
- (IBAction)onPressingMinusButton:(UIButton *)sender;
- (IBAction)onPressingStarButton:(UIButton *)sender;
- (IBAction)onPressingClearButton:(UIButton *)sender;
- (IBAction)onPressingByButton:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *textField;


@end
